<?php
/*
Plugin Name: Valkyries Horn Site Plugin
Plugin URI: https://valkyrieshorn.com
Description: Site-specific Plugin.
Version: 1.0.20190307
Author: Dave Royer
Author URI: https://twitter.com/daveroyer
License: GPL2
*/

require 'updater.php';


/* ************************************
 * Pull in the style-sheet changes
 * ************************************ */

function valkyrieshorn_load_stylesheet() {
    wp_enqueue_style( 'valkyrieshorn',  plugin_dir_url( __FILE__ ) . 'assets/style.css' );
}
add_action('wp_enqueue_scripts', 'valkyrieshorn_load_stylesheet');


function valkyrieshorn_custom_fonts ( $system_fonts ) {
  $system_fonts[ 'BourbonGrotesque' ] = array(
    'fallback' => 'Verdana, Arial, sans-serif',
    'weights' => array(
        '400',
    ),
  );
  return $system_fonts;
}
//Add to Beaver Builder Theme Customizer
add_filter( 'fl_theme_system_fonts', 'valkyrieshorn_custom_fonts' );
//Add to Page Builder modules
add_filter( 'fl_builder_font_families_system', 'valkyrieshorn_custom_fonts' );