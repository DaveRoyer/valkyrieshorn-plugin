<?php
/* *************************
 *
 * Updater for plugin
 * 
 * **************************/

require 'vendor/plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/DaveRoyer/valkyrieshorn-plugin/',
	__FILE__,
	'valkyrieshorn-plugin'
);

//Optional: If you're using a private repository, specify the access token like this:
// $myUpdateChecker->setAuthentication('your-token-here');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');